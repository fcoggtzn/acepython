def hello_world_2(name: str):
    """
    This is a hello world function
    :param name:  This is the __message to show
    :return:  noting
    """
    print(name + "___ 2 ___")


