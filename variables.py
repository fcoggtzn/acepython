import constant
c = "Hola mundo"
print(type(c))
c = 'Hola mundo'
print(type(c))
c = 'º'
print(type(c))
c = 5.0
print(type(c))


def suma(x,y):
    print(x+y)

suma(5,c)


x = y = z = 5
print(x)


THIS_IS_CONSTANT = 10

print(THIS_IS_CONSTANT)
THIS_IS_CONSTANT = "Hola"
print(THIS_IS_CONSTANT)


hi_var = (None, True, Ellipsis, NotImplemented, False)

if hi_var is None:
    print("hi")


print(constant.HOST)

print(vars())


