def sumar(x, y):
    return x + y


sumar2 = lambda x, y: x + y

print(sumar2(4, 5))

# numero es impar
impar = lambda numero: numero % 2 != 0

print(impar(8))
print(impar(7))

# funcion hola --> aloh
invertir = lambda cadena: cadena[::-1]
print(invertir("Hola Mundo"))

cadena = "Hola mundo"

print(cadena[2::])

# palindromo
palindromo = lambda cadena: cadena.lower().replace(' ', '') == cadena.lower().replace(' ', '')[:: -1]

print(palindromo("Anita lava latina"))
