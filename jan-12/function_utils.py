import FirstClass

'''def saludar():
    print('Hola soy una función')

def super_funcion(funcion):
    funcion()

funcion = saludar  # Asignamos la función a una variable!

super_funcion(funcion)
'''
'''
def funcion_a(funcion_b):
    def funcion_c(*args, **kwargs):
        print('Antes de la ejecución de la función a decorar')
        result = funcion_b(*args, **kwargs)
        print('Después de la ejecución de la función a decorar')

        return result

    return funcion_c

@funcion_a
def suma(a, b):
    print('Dentro de la funcion')
    return a + b


print(suma(3,4))'''

cont = 0
#____________________________
def my_custome_decorator(function):
    def wrapper(*args, **kwargs):
        global cont
        cont = cont + 1
        print("xxxxxxxxx Step núm: "+str(cont)+" xxxxxxxxxxx")
        return function(*args, **kwargs)

    return wrapper


@my_custome_decorator
def resta(x, y):
    return x -y

@my_custome_decorator
def suma(x, y):
    return x + y


'''
print(resta(3,4))
print(suma(6,8))
print(resta(4,3))'''