import function_utils
class FirstClass:
    num_instancia = 0

    def __init__(self):
        #self.num_instancia = FirstClass.inc()

        print("Constructor" + str(self.num_instancia))

    @classmethod
    def inc(cls):
        cls.num_instancia = cls.num_instancia + 1
        return cls.num_instancia

    @staticmethod
    def say_hi(name):
        print(" Welcome :"+name)

    @staticmethod
    def add(x,y):
        return function_utils.suma(x,y)




#FirstClass.say_hi("Fco")
FirstClass.num_instancia = FirstClass.num_instancia + 100