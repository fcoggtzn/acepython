from mysql.connector import connect, Error

try:
    with connect(
        host="localhost",
        user="root",
        password="root",
        database="pythonace",
    ) as connection:
        create_db_query = "insert into calificacion (calificacion , alumno_idalumno) values (%s , %s) "
        calif=[(8,6),(10,1)]
        with connection.cursor() as cursor:
            cursor.executemany(create_db_query, calif)
            connection.commit()
except Error as e:
    print(e)

