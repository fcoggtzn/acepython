from mysql.connector import connect, Error

try:
    with connect(
        host="localhost",
        user="root",
        password="root",
        database="pythonace",
    ) as connection:
        create_db_query = "SELECT  * from alumno a  join  Calificacion c on (c.alumno_idalumno = a.idalumno)"
        with connection.cursor() as cursor:
            cursor.execute(create_db_query)
            for alumno in cursor.fetchall():
              print(alumno)
except Error as e:
    print(e)

