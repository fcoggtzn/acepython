from mysql.connector import connect, Error

try:
    with connect(
        host="localhost",
        user="root",
        password="root",
    ) as connection:
        create_db_query = "CREATE DATABASE temporal_db"
        with connection.cursor() as cursor:
            cursor.execute(create_db_query)
except Error as e:
    print(e)

