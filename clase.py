

class MyFirstClass:
    __message = " Hello world "

    def __init__(self):
        print("hi")

    def set_message(self, message: str):
        """

        :param message:
        :return:
        """
        self.__message = message

    @staticmethod
    def say_hello2():
        print("Welcome")

    @classmethod
    def say_hello3(cls):
        cls.__message = "Welcome"


    def say_hello(self):
        print(self.__message)
'''

my_first_class = MyFirstClass()
my_first_class.set_message("hello")
my_first_class.say_hello()

MyFirstClass.say_hello2()'''
MyFirstClass.say_hello3()

my_first_class2 = MyFirstClass()
my_first_class2.say_hello()


